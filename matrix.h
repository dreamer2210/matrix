#pragma once
#include <initializer_list>
#include <ostream>
#include <istream>

typedef unsigned long long int size_type;

class matrix
{
private:
    double** mat;
    size_type countRows;
    size_type* countCols;
public:
    ~matrix();
    matrix(const std::initializer_list<const std::initializer_list<double>>& obj);
    matrix(const matrix& obj);

    // if matrix is not rectangle or square
    matrix(size_type rows, const std::initializer_list<size_type>& cols);
    matrix(size_type rows, size_type* cols);
    // if matrix is rectangle or square
    matrix(size_type rows, size_type cols);
    // if matrix is a vector
    matrix(const std::initializer_list<size_type>& obj);
    matrix(size_type rows);
    // if matrix is empty ('mat', 'countRows', 'countCols' equals 0)
    matrix();
    
    // if matrix is rectangle or square (UNSAFE)
    matrix transpose() const;

    // returned new matrix without row and col
    matrix cut(size_type numRow, size_type numCol) const;

    // inset value in matrix
    matrix insetVal(size_type numRow, size_type numCol, double value) const;
    // inset row in matrix
    matrix insetRow(size_type numRow, size_type cols, double* row) const;

    matrix inverse() const;

    matrix addRow(size_type rows, size_type* cols) const;
    matrix addRow(size_type cols) const;

    // determinant of matrix 2x2
    double determ2x2() const;
    // determinant of matrix 3x3 (triangle method)
    double determ3x3() const;
    // special algorithm (matrix must be minimum 2x2)
    double determNxN() const;
    // safe method
    double determ_s() const;

    matrix addition() const;

    friend matrix solve(const matrix& obj, const matrix& answer);

    double* getRow(size_type row) const;
    double* getCol(size_type col) const;
    double getCountRows() const;
    double getCountCols(size_type col) const;

    double* operator [] (size_type i);
    const double* operator [] (size_type i) const;

    matrix& operator = (const std::initializer_list<const std::initializer_list<double>>& obj);
    matrix& operator = (const std::initializer_list<double>& obj);
    matrix& operator = (const matrix& obj);

    // works if a.countRows == b.countCols[0] and both of them is rectangle or square
    friend matrix operator * (const matrix& a, const matrix& b);
    friend matrix operator * (const matrix& a, double b);
    friend matrix operator * (double a, const matrix& b);

    // works if a.countRows == b.countRows and every a.countCols[i] == b.countCols[i]
    friend matrix operator + (const matrix& a, const matrix& b);
    friend matrix operator + (const matrix& a, double b);
    friend matrix operator + (double a, const matrix& b);    

    // works if a.countRows == b.countRows and every a.countCols[i] == b.countCols[i]
    friend matrix operator - (const matrix& a, const matrix& b);
    friend matrix operator - (const matrix& a, double b);
    friend matrix operator - (double a, const matrix& b); 

    // matrix * 1/matrix. works if a.countRows == b.countCols[0] and both of them is rectangle or square
    friend matrix operator / (const matrix& a, const matrix& b);
    friend matrix operator / (const matrix& a, double b);
    friend matrix operator / (double a, const matrix& b); 

    friend std::ostream& operator << (std::ostream& stream, const matrix& obj);
    // matrix MUST BE INITIALIZED
    friend std::istream& operator >> (std::istream& stream, matrix& obj);
};