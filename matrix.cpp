#include "matrix.h"

matrix::matrix(const std::initializer_list<const std::initializer_list<double>>& obj)
{
    size_type i = 0, j = 0;

    this->countRows = obj.size();
    this->countCols = new size_type[countRows];

    this->mat = new double*[countRows];

    for(auto rows = obj.begin(); rows < obj.end(); ++rows, ++i)
    {
        this->countCols[i] = rows->size();
        this->mat[i] = new double[rows->size()];

        for(auto cols = rows->begin(); cols < rows->end(); ++cols, ++j)
            mat[i][j] = cols[0];

        j = 0;
    }
}

matrix::matrix(const matrix& obj)
{
    this->countRows = obj.countRows;
    this->countCols = new size_type[countRows];

    this->mat = new double*[countRows];

    for(size_type i = 0; i < countRows; ++i)
    {
        this->countCols[i] = obj.countCols[i];

        this->mat[i] = new double[countCols[i]];

        for(size_type j = 0; j < countCols[i]; ++j)
        {
            this->mat[i][j] = obj.mat[i][j];
        }
    }
}

matrix::matrix(size_type rows, const std::initializer_list<size_type>& cols)
{
    this->countRows = rows;
    this->countCols = new size_type[rows];

    this->mat = new double*[rows];
    size_type i = 0;

    for(auto& obj : cols)
    {
        countCols[i] = obj;

        this->mat[i] = new double[obj];

        ++i;
    }
}

matrix::matrix(size_type rows, size_type* cols)
{
    this->countRows = rows;
    this->countCols = new size_type[rows];

    this->mat = new double*[rows];

    for(size_type i = 0; i < rows; ++i)
    {
        countCols[i] = cols[i];

        this->mat[i] = new double[cols[i]];
    }
}

matrix::matrix(size_type rows, size_type cols)
{
    this->countRows = rows;
    this->countCols = new size_type[countRows];

    this->mat = new double*[rows];

    for(size_type i = 0; i < rows; ++i)
    {
        this->mat[i] = new double[cols];
        this->countCols[i] = cols;
    }
}

matrix::matrix(const std::initializer_list<size_type>& obj)
{
    size_type i = 0;

    this->countRows = 1;
    this->countCols = new size_type[1];
    this->countCols[0] = obj.size();

    this->mat = new double*[countRows];
    this->mat[0] = new double[obj.size()];

    for(auto val : obj)
    {
        mat[0][i] = val;
        ++i;
    }
}

matrix::matrix(size_type rows)
{
    this->countRows = rows;
    this->countCols = new size_type[rows];

    this->mat = new double*[rows];

    for(size_type i = 0; i < rows; ++i)
    {
        this->countCols[i] = 1;

        this->mat[i] = new double[1];
    }
}

matrix::matrix()
{
    this->countRows = 0;
    this->countCols = 0;

    this->mat = 0;
}

matrix::~matrix()
{
    if(countRows != 0)
    {
        for(size_type i = 0; i < countRows; ++i)
            delete[] mat[i];
            
        delete[] mat;
        delete[] countCols;
    }
}

matrix matrix::transpose() const
{
    matrix result(this->countCols[0], this->countRows);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[0]; ++j)
        {
            result.mat[i][j] = this->mat[j][i];
        }
    }

    return result;
}

matrix matrix::cut(size_type numRow, size_type numCol) const
{
    matrix result(this->countRows - 1, this->countCols[0] - 1);
    size_type iRes, jRes, iThis, jThis;

    for(iThis = 0, iRes = 0; iRes < result.countRows; ++iThis, ++iRes)
    {
        if(iThis == numRow)
            ++iThis;

        for(jThis = 0, jRes = 0; jRes < result.countCols[0]; ++jThis, ++jRes)
        {
            if(jThis == numCol)
                ++jThis;
            
            result.mat[iRes][jRes] = this->mat[iThis][jThis];
        }
    }

    return result;
}

matrix matrix::inverse() const
{
    matrix result(this->countRows, this->countCols);
    short int one = 1;
    double determ = this->determ_s();

    if(determ == 0)
        return 0;

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result[i][j] = one * this->cut(i, j).determ_s();

            one = -one;
        }
    }

    result = result.transpose();
    result = result * (1/determ);

    return result;
}

matrix matrix::addRow(size_type rows, size_type* cols) const
{
    size_type rowsResult = this->countRows + rows;
    size_type* colsResult = new size_type[rowsResult];

    for(size_type i = 0; i < this->countRows; ++i)
        colsResult[i] = this->countCols[i];
    
    for(size_type i = this->countRows, j = 0; i < rowsResult; ++i, ++j)
        colsResult[i] = cols[j];

    matrix result(rowsResult, colsResult);
    size_type i, j;

    for(i = 0; i < this->countRows; ++i)
    {
        for(j = 0; j < this->countCols[i]; ++j)
        {
            result[i][j] = this->mat[i][j];
        }
    }

    for(; i < result.getCountRows(); ++i)
    {
        for(j = 0; j < result.getCountCols(i); ++j)
        {
            result[i][j] = 0;
        }
    }

    delete[] colsResult;

    return result;
}

matrix matrix::addRow(size_type cols) const
{
    size_type colsArr[1] = {cols};

    return addRow(1, colsArr);
}

double matrix::determ2x2() const
{
    return this->mat[0][0] * this->mat[1][1] - (this->mat[0][1] * this->mat[1][0]);
}

double matrix::determ3x3() const
{
    double triangle1 = this->mat[0][0] * this->mat[1][1] * this->mat[2][2] +
                        this->mat[0][1] * this->mat[2][0] * this->mat[1][2] +
                        this->mat[1][0] * this->mat[0][2] * this->mat[2][1];
    double triangle2 = this->mat[0][2] * this->mat[1][1] * this->mat[2][0] +
                        this->mat[1][0] * this->mat[0][1] * this->mat[2][2] +
                        this->mat[0][0] * this->mat[2][1] * this->mat[1][2];
    
    return triangle1 - triangle2;
}

double matrix::determNxN() const
{
    short int one = 1;
    double result = 0;
    matrix minor(this->countRows - 1, this->countCols[0] - 1);

    for(size_type j = 0; j < this->countCols[0]; ++j)
    {
        minor = this->cut(0, j);

        result += one * this->mat[0][j] * minor.determ_s();

        one = -one;
    }

    return result;
}

double matrix::determ_s() const
{
    double result;

    if(this->countRows >= 2 && this->countCols[0] >= 2)
    {
        if(this->countRows == 2 && this->countCols[0] == 2)
            result = this->determ2x2();
        else if(this->countRows == 3 && this->countCols[0] == 3)
            result = this->determ3x3();
        else
            result = this->determNxN();
    }
    else
        return this->mat[0][0];

    return result;
}

matrix matrix::addition() const
{
    matrix result(this->countRows, this->countCols);

    for(size_type i = 0; i < this->getCountRows(); ++i)
    {
        for(size_type j = 0; j < this->getCountCols(i); ++j)
        {
            if((i + j) % 2 == 0)
                result[i][j] = this->cut(i, j).determ_s();
            else
                result[i][j] = -this->cut(i, j).determ_s();
        }
    }

    return result;
}

matrix solve(const matrix& obj, const matrix& answer)
{
    return obj.inverse() * answer;
}

matrix matrix::insetVal(size_type numRow, size_type numCol, double value) const
{
    matrix result = *this;

    double* row = this->getRow(numRow);
    size_type count = this->getCountCols(numCol);

    delete this->mat[numRow];
    ++this->countCols[numRow];

    this->mat[numRow] = new double[count + 1];

    for(size_type i = 0, j = 0; i < getCountCols(numRow); ++i)
    {
        if(i == numCol)
        {
            this->mat[numRow][i] = value;
        }
        else
        {
            this->mat[numRow][i] = row[j];
            ++j;
        }
    }

    delete[] row;

    return result;
}

matrix matrix::insetRow(size_type numRow, size_type cols, double* newRow) const
{
    matrix result = this->addRow(cols);
    size_type col;
    double* row;

    //
    row = result.getRow(numRow);
    delete[] result[numRow];

    result.mat[numRow] = new double[cols];

    for(size_type i = 0; i < cols; ++i)
        result[numRow][i] = newRow[i];
    //

    for(size_type i = numRow + 1; i < result.countRows; ++i)
    {
        col = result.getCountCols(i);

        delete[] result.mat[i];

        result.mat[i] = new double[col];

        for(size_type j = 0; j < result.countCols[i]; ++j)
            result[i][j] = row[j];
        
        delete[] row;
        row = result.getRow(i);
    }

    delete[] row;

    return result;
}

double* matrix::getRow(size_type row) const
{
    double* line = new double[this->countCols[row]];

    for(size_type i = 0; i < countCols[row]; ++i)
        line[i] = mat[row][i];
    
    return line;
}

double* matrix::getCol(size_type col) const
{
    double* line = new double[this->countRows];

    for(size_type i = 0; i < countRows; ++i)
        line[i] = this->mat[i][col];

    return line;
}

double matrix::getCountRows() const
{
    return this->countRows;
}

double matrix::getCountCols(size_type col) const
{
    return this->countCols[col];
}

double* matrix::operator[] (size_type i)
{
    return this->mat[i];
}

const double* matrix::operator[] (size_type i) const
{
    return mat[i];
}

matrix& matrix::operator = (const std::initializer_list<const std::initializer_list<double>>& obj)
{
    size_type i = 0, j = 0;

    if(countRows != 0)
    {
        for(;i < this->countRows; ++i)
            delete[] this->mat[i];
        
        delete[] this->countCols;

        i = 0;
    }

    this->countRows = obj.size();
    this->countCols = new size_type[countRows];

    this->mat = new double*[countRows];

    for(auto rows = obj.begin(); rows < obj.end(); ++rows, ++i)
    {
        this->countCols[i] = rows->size();
        this->mat[i] = new double[rows->size()];

        for(auto cols = rows->begin(); cols < rows->end(); ++cols, ++j)
            mat[i][j] = cols[0];

        j = 0;
    }

    return *this;
}

matrix& matrix::operator = (const std::initializer_list<double>& obj)
{
    size_type i = 0;

    if(this->countRows != 0)
    {
        for(;i < countRows; ++i)
            delete[] mat[i];

        delete[] countCols;

        i = 0;
    }

    this->countRows = 1;
    this->countCols = new size_type[1];
    this->countCols[0] = obj.size();

    this->mat = new double*[countRows];
    this->mat[0] = new double[obj.size()];

    for(auto val : obj)
    {
        mat[0][i] = val;
        ++i;
    }

    return *this;
}

matrix& matrix::operator = (const matrix& obj)
{
    if(this->countRows != 0)
    {
        for(size_type i = 0; i < countRows; ++i)
            delete[] mat[i];

        delete[] countCols;
    }

    this->countRows = obj.countRows;
    this->countCols = new size_type[countRows];

    this->mat = new double*[countRows];

    for(size_type i = 0; i < countRows; ++i)
    {
        this->countCols[i] = obj.countCols[i];
        this->mat[i] = new double[countCols[i]];

        for(size_type j = 0; j < countCols[i]; ++j)
            this->mat[i][j] = obj.mat[i][j];
    }

    return *this;
}

matrix operator * (const matrix& a, const matrix& b)
{
    matrix result(a.countRows, b.countCols[0]);

    for (int i = 0; i < a.countRows; ++i)
    {
        for (int j = 0; j < b.countCols[0]; ++j)
        {
            result.mat[i][j] = 0;

            for (int k = 0; k < a.countCols[0]; ++k)
                result.mat[i][j] += a.mat[i][k] * b.mat[k][j];
        }
    }

    return result;
}

matrix operator * (const matrix& a, double b)
{
    matrix result = a;

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a.mat[i][j] * b;
        }
    }

    return result;
}

matrix operator * (double a, const matrix& b)
{
    matrix result = b;

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = b.mat[i][j] * a;
        }
    }

    return result;
}

matrix operator + (const matrix& a, const matrix& b)
{
    matrix result(a.countRows, a.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a.mat[i][j] + b.mat[i][j];
        }
    }

    return result;
}

matrix operator + (const matrix& a, double b)
{
    matrix result(a.countRows, a.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a.mat[i][j] + b;
        }
    }

    return result;
}

matrix operator + (double a, const matrix& b)
{
    matrix result(b.countRows, b.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a + b.mat[i][j];
        }
    }

    return result;
}

matrix operator - (const matrix& a, const matrix& b)
{
    matrix result(a.countRows, a.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a.mat[i][j] - b.mat[i][j];
        }
    }

    return result;
}

matrix operator - (const matrix& a, double b)
{
    matrix result(a.countRows, a.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a.mat[i][j] - b;
        }
    }

    return result;
}

matrix operator - (double a, const matrix& b)
{
    matrix result(b.countRows, b.countCols);

    for(size_type i = 0; i < result.countRows; ++i)
    {
        for(size_type j = 0; j < result.countCols[i]; ++j)
        {
            result.mat[i][j] = a - b.mat[i][j];
        }
    }

    return result;
}

matrix operator / (const matrix& a, const matrix& b)
{
    matrix result = a;

    result = a * b.inverse();

    return result;
}

matrix operator / (const matrix& a, double b)
{
    matrix result = a;

    for(size_type i = 0; i < result.getCountRows(); ++i)
    {
        for(size_type j = 0; j < result.getCountCols(i); ++j)
        {
            result[i][j] = a[i][j] / b;
        }
    }

    return result;
}

matrix operator / (double a, const matrix& b)
{
    matrix result = b;

    for(size_type i = 0; i < result.getCountRows(); ++i)
    {
        for(size_type j = 0; j < result.getCountCols(i); ++j)
        {
            result[i][j] = a / b[i][j];
        }
    }

    return result;
}  

std::ostream& operator << (std::ostream& stream, const matrix& obj)
{
    if(obj.countRows == 0)
    {
        stream << "ERROR! Matrix is empty.";
        return stream;
    }

    for(size_type i = 0; i < obj.countRows; ++i)
    {
        for(size_type j = 0; j < obj.countCols[i]; ++j)
        {
            stream << obj.mat[i][j] << '\t';
        }

        stream << std::endl;
    }

    return stream;
}

std::istream& operator >> (std::istream& stream, matrix& obj)
{
    if(obj.getCountRows() == 0)
        return stream;

    for(size_type i = 0; i < obj.getCountRows(); ++i)
    {
        for(size_type j = 0; j < obj.getCountCols(i); ++j)
        {
            stream >> obj[i][j];
        }
    }

    return stream;
}